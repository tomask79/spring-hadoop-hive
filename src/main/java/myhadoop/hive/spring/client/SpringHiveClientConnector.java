package myhadoop.hive.spring.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.jdbc.core.JdbcTemplate;

/**
 * Spring Hive connector. 
 * 
 * @author tomask79
 *
 */
public class SpringHiveClientConnector {
	private JdbcTemplate jdbcTemplate;
	
	public void setJdbcTemplate(final JdbcTemplate template) { 
		this.jdbcTemplate = template; 
	}
	
	public List<String> getTables() {
		List<Map<String, Object>> rows = jdbcTemplate.queryForList("show tables");
		List<String> tables = new ArrayList<String>();
	    for (Map<String, Object> row : rows) {
	    	for (String key : row.keySet()) {
	    	    tables.add((String) row.get(key));
	    	}
	    }
	    
	    return tables;
	}
}
