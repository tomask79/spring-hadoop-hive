package myhadoop.hive.spring.demo;

import java.util.List;

import myhadoop.hive.spring.client.SpringHiveClientConnector;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ClientDemo {
	public static void main(String args[]) {
        ApplicationContext ctx = new 
        		ClassPathXmlApplicationContext("applicationContext.xml");
        
        SpringHiveClientConnector clientConnector = (SpringHiveClientConnector) 
        		ctx.getBean("hiveClientConnector");
        
        final List<String> dbs = clientConnector.getTables();
        for (String dbName: dbs) {
        	System.out.println("TableName: "+dbName);
        }
	}
}
