This repo contains example of integrating the 

**Apache Hadoop(2.6.0) + Apache Hive(1.0.0) with Spring framework.**

To run the demo:


```
1) Run the Hive 2 server.
2) Launch the ClientDemo class.
```

If you have everything configured correctly then you should see 
**all tables in the default hive database.** (in my case)

```
TableName: hivetabletest
TableName: hivetabletest2
TableName: test
TableName: test2
```

Note: I'm a little disappointed with support of Hive in Spring. Why such perfect thing like HiveClientCallBack uses outdated Apache Hive API? If you just want to query Hive via Spring Framework, JdbcTemplate looks as fine way to me...Other options don't make much sense.